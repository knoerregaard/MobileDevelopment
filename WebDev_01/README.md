# 1. Læringsmål for WebDev_01

Den studerende kan strukture HTML dokumenter således, at dokumenterne fremstår semantiske. Dokumenterne skal være deskriptive. 

# 2. Forløb 
Start med at se videoerne under punktet Forberedelse. Det kan godt være at du allerede er bekendt med HTML og elementer, men der er bestemt nye spændende emner for selv de mere erfarne.
Når du har set videoerne kan du påbegynde øvelsen. Se punktet Øvelse.



# 3. Forberedelse
Skitse anviser retningslinjer for designet af den mobile applikation i skal udvikle.
#### Welcome
https://www.lynda.com/HTML-tutorials/Welcome/182177/370804-4.html?
### What are web semantics
https://www.lynda.com/HTML-tutorials/What-web-semantics/182177/370807-4.html?
#### Semantic HTML5
https://www.lynda.com/HTML-tutorials/Semantic-HTML5/182177/370808-4.html?
#### HTML5 content models
https://www.lynda.com/HTML-tutorials/HTML5-content-models/182177/370809-4.html?
#### Structuring HTML5 documents
https://www.lynda.com/HTML-tutorials/Structuring-HTML5-documents/182177/370811-4.html?
#### Defining HTML5 documents (3m 46 s)
https://www.lynda.com/HTML-tutorials/Defining-HTML5-documents/182177/370812-4.html?
#### Supporting legacy browsers (4m 16s)
https://www.lynda.com/HTML-tutorials/Supporting-legacy-browsers/182177/370813-4.html?
#### Organizing content (4m 26s)
https://www.lynda.com/HTML-tutorials/Organizing-content/182177/370814-4.html?
#### Creating document sections (7m 44s)
https://www.lynda.com/HTML-tutorials/Creating-document-sections/182177/370815-4.html?
#### Identifying the main content (2m 17s)
https://www.lynda.com/HTML-tutorials/Identifying-main-content/182177/370816-4.html?
#### Using headings properly (8m 51s)
https://www.lynda.com/HTML-tutorials/Using-headings-properly/182177/370817-4.html?
#### Building navigation (3m 56s)
https://www.lynda.com/HTML-tutorials/Building-navigation/182177/370818-4.html?
#### Properly nesting structure (6m 32s)
https://www.lynda.com/HTML-tutorials/Properly-nesting-structure/182177/370819-4.html?
#### Structuring headers (7m 33s)
https://www.lynda.com/HTML-tutorials/Structuring-headers/182177/370820-4.html?
#### Structuring footers (4m 11s)
https://www.lynda.com/HTML-tutorials/Structuring-footers/182177/370821-4.html?
#### Checking document structure (4m 1s)
https://www.lynda.com/HTML-tutorials/Checking-document-structure/182177/370822-4.html?
#### Sectioning roots (3m 7s)
https://www.lynda.com/HTML-tutorials/Sectioning-roots/182177/370823-4.html?

# 4. Øvelse
#### 1
Hent projekt  https://gitlab.com/knoerregaard/MobilDev.git (Projektet indeholder blot et par filer)
git clone https://gitlab.com/knoerregaard/MobilDev.git

#### 2 
Implementer index.html, således at dokumentet er i overensstemmelse med retningslinjerne anvist. Husk at der siden ikke vil se særlig præsentabel ud, eftersom vores style sheet ikke er applikeret på nuværende tidspunkt.

#### 3 
Push løsningen til Gitlab i et eget offentligt projekt, hvor dine kollegaer kan bidrage med input til evt. rettelser eller komme med synspunkter.